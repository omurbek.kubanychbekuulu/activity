<?php

namespace App\Repository;

use App\Entity\VisitEtity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VisitEtity|null find($id, $lockMode = null, $lockVersion = null)
 * @method VisitEtity|null findOneBy(array $criteria, array $orderBy = null)
 * @method VisitEtity[]    findAll()
 * @method VisitEtity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitEtityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VisitEtity::class);
    }

    //получение визита пользователя
    public function getVisits(string $token,$page = 1,$limit = 2): array {
        $page = $page <= 0 ? $page = 1 : $page;
        return $this->createQueryBuilder('v')
            ->select('v.url,COUNT(v.id) as visitCount,MAX(v.time) as lastVisit')
            ->where('v.userToken =:token')
            ->groupBy('v.url')
            ->setParameter('token',$token)
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit)
            ->getQuery()->getArrayResult();
    }
}
