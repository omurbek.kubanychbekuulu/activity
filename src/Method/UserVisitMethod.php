<?php

namespace App\Method;

use App\Services\VisitService;
use Symfony\Component\Validator\Constraint;
use Yoanm\JsonRpcParamsSymfonyValidator\Domain\MethodWithValidatedParamsInterface;
use Yoanm\JsonRpcServer\Domain\JsonRpcMethodInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Required;

class UserVisitMethod implements JsonRpcMethodInterface, MethodWithValidatedParamsInterface
{

    private $visitService;

    public function __construct(VisitService $visitService)
    {
        $this->visitService = $visitService;
    }


    public function apply(array $paramList = null): array
    {
        $res = ['message'=>'Access denied'];
        if ($this->visitService->isAccess($paramList['accessToken'],$paramList['userToken'])) {
            $res = $this->visitService->createVisit($paramList);
        }

        return $res;
    }

    /**
     * @return Constraint
     * Валидация параметров запроса
     */
    public function getParamsConstraint(): Constraint
    {
        return new Collection(['fields' => [
            'url' => new Required([
                new Length(['min' => 3, 'max' => 255])
            ]),
            'userToken' => new Required(),
            'accessToken' => new Required()
        ]]);
    }
}