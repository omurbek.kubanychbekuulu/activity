<?php

namespace App\Services;

use App\Entity\VisitEtity;
use Doctrine\ORM\EntityManagerInterface;

class VisitService
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    // Сохранение данных визита в бд
    public function createVisit(array $requestData): array
    {
        $res = ['success' => true];
        try {
            $visit = new VisitEtity();
            $visit->setUrl($requestData['url']);
            $visit->setUserToken($requestData['userToken']);
            $this->em->persist($visit);
            $this->em->flush();
        } catch (\Exception $em) {
            $tes['success'] = false;
        }
        return $res;
    }

    public function getUserVisits($token,$page){
        $visitRepo = $this->em->getRepository(VisitEtity::class);
        return $visitRepo->getVisits(trim($token),$page);
    }

    public function isAccess(string $accessToken,$userToken): bool{
        $salt = $_ENV['APP_SALT'];
        $serverToken =  $salt . ":" . MD5($salt . ":" . $_ENV['APP_SECRET'] . ":" . $userToken);
        return $serverToken == $accessToken;
    }

}